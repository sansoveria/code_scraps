%%
% matlab code for writting mat into txt file and parse txt file
% written in 2019-01-04 by D.Lee


%% data and parameters
filename = 'filename.txt';
x = zeros(100,10);


%% write mat into txt file
% delimiter space ' '
dlmwrite('filename.txt', x, ' '); 

% delimiter tab '\t'
dlmwrite('filename.txt', x, '\t'); 

% data with specific format (ex. %3.4f)
% to be updated


%% parse txt file into mat
% delimiter space ' '
data = dlmread(filename, ' ');

% delimiter tab '\t'
data = dlmread(filename, '\t');
