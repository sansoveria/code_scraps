%%
% matlab code for plot formatting
% written in 2019-01-04 by D.Lee

%% data
x = 0:0.1:10;
y = 0:0.1:10;
x1 = 0;
x2 = 1;

%% plot and formatting
f = figure('Position',[0,0,800,500])
plot(x, y,'LineWidth', 1.5);
xlim([x1,x2]);
xlabel('Step (count)');
ylabel('Error');

% if you want to write labels with latex format
% ylabel('$$\|Error\|$$','interpreter', 'latex');

set(gca,'LineWidth',1.5);
set(gca,'FontSize',20);
set(gca,'xtick',0:x2/5:x2);
set(gca, 'fontweight','bold');
