////////////////////////////////////////////////////////////////////////////////
// C++ code for parsing text file with ' ' delimiter
// written in 2019-01-05 by D.Lee
////////////////////////////////////////////////////////////////////////////////
// Requirements (libraries)
// -
////////////////////////////////////////////////////////////////////////////////

#include <fstream>

int txt_parser(std::string filename, double *x) {
    int size = 255 /* need to be changed*/
    
	std::ifstream infile(filename, std::ifstream::in);
	if (infile.is_open()) {
		printf("%s is open\r\n", filename.c_str());

		int i = 0;
		while (infile >> x[3*i] >> x[3*i+1] >> x[3*i+2]) {
			i++;
			if (i >= size) {
				break;
			}
		}
		infile.close();
		return i;
	}
	else {
		printf("%s cannot be open\r\n", filename.c_str());
		return 0;
	}
}


int txt_writter(std::string filename, double *x) {
    int size = 255 /* need to be changed*/

    std::ofstream txt_data;
	txt_data.open(filename.c_str());
	for (int i = 0; i < size; i++){
		char buffer[1000];      /* can be larger*/
		sprintf_s(buffer, "%.5f %.5f %.5f \r\n", x[3*i], x[3*i+1], x[3*i+2]);
		txt_data << buffer;
	}
	txt_data.close();
}