////////////////////////////////////////////////////////////////////////////////
// C++ code to utilize qpOASES
// written in 2018-11-16 by D.Lee
////////////////////////////////////////////////////////////////////////////////
// Reference
// -
////////////////////////////////////////////////////////////////////////////////
// Requirements (libraries)
// qpOASES
////////////////////////////////////////////////////////////////////////////////

#ifndef __USE_LONG_INTEGERS__	// required for qpOASES
#define __USE_LONG_INTEGERS__
#endif

#ifndef __USE_LONG_FINTS__		// required for qpOASES
#define __USE_LONG_FINTS__
#endif

#include "DefineConstant.h"
#include <qpOASES.hpp>
#include <Eigen/Core>

template<int nVariables, int nConditions>
class qpOASESWrapper{
public:
	typedef Eigen::Matrix<double, nVariables, 1> VarVec;			// for g, lb, ub
	typedef Eigen::Matrix<double, nConditions, 1> CondVec;			// for lbA, ubA
	typedef Eigen::Matrix<double, nConditions, nVariables> CondMat;	// for A
	typedef Eigen::Matrix<double, nVariables, nVariables> Hessian;	// for H

	void initQPSolver(){
		_solver = qpOASES::QProblem(nVariables, nConditions, qpOASES::HST_IDENTITY);
		qpOASES::Options options;
		options.printLevel = qpOASES::PL_NONE;
		_solver.setOptions(options);
		_INITIAL_PROBLEM = true;
	}

	void initLPSolver(){
		_solver = qpOASES::QProblem(nVariables, nConditions, qpOASES::HST_ZERO);
		qpOASES::Options options;
		options.printLevel = qpOASES::PL_NONE;
		_solver.setOptions(options);
		_INITIAL_PROBLEM = true;
	}

	void resetSolver(){
		_INITIAL_PROBLEM = true;
	}

	bool solveQuadraticProblem(Hessian H, VarVec g, CondMat A, VarVec lb, VarVec ub, CondVec lbA, CondVec ubA, VarVec& solution){
		qpOASES::int_t nWSR = 100;
		qpOASES::real_t _H[nVariables*nVariables];
		qpOASES::real_t _g[nVariables];
		qpOASES::real_t _lb[nVariables];
		qpOASES::real_t _ub[nVariables];
		qpOASES::real_t _lbA[nConditions];
		qpOASES::real_t _ubA[nConditions];
		qpOASES::real_t _A[nConditions*nVariables];

		for(int i=0; i<nConditions; i++){
			for(int j=0; j<nVariables; j++){
				_A[i*nVariables+j] = A(i,j);
				if(i==0){
					for(int h=0; h<nVariables; h++){
						_H[h*nVariables+j] = H(h,j);
					}
					_lb[j] = lb(j);
					_ub[j] = ub(j);
					_g[j] = g(j);
				}
			}
			_lbA[i] = lbA(i);
			_ubA[i] = ubA(i);
		}

		//if (_INITIAL_PROBLEM){
			qpOASES::returnValue res = _solver.init(_H, _g, _A, _lb, _ub, _lbA, _ubA, nWSR, 0);
			//if (res == qpOASES::SUCCESSFUL_RETURN) _INITIAL_PROBLEM = false;
		//}
		//else{
		//	qpOASES::returnValue res = _solver.hotstart(H, _g, _A, _lb, _ub, _lbA, _ubA, nWSR, 0);
		//}

		if (res == qpOASES::SUCCESSFUL_RETURN){
			qpOASES::real_t xOpt[nVariables];
			_solver.getPrimalSolution(xOpt);
			//printf("\nxOpt = [ %e, %e ];  objVal = %e\n\n", xOpt[0], xOpt[1], _solver.getObjVal());
			//std::cout << xOpt[0] << ", " << xOpt[1] << std::endl;
			for(int i=0; i<nVariables; i++){
				solution(i) = xOpt[i];
			}
			return true;
		}
		else{
			solution = VarVec::Zero();
			printf("[qpOASES] Failed to find solution");
			return false;
		}
	}

	bool solveLinearProblem(VarVec g, CondMat A, VarVec lb, VarVec ub, CondVec lbA, CondVec ubA, VarVec& solution){
		qpOASES::int_t nWSR = 100;
		qpOASES::real_t _g[nVariables];
		qpOASES::real_t _lb[nVariables];
		qpOASES::real_t _ub[nVariables];
		qpOASES::real_t _lbA[nConditions];
		qpOASES::real_t _ubA[nConditions];
		qpOASES::real_t _A[nConditions*nVariables];

		for(int i=0; i<nConditions; i++){
			for(int j=0; j<nVariables; j++){
				_A[i*nVariables+j] = A(i,j);
				if(i==0){
					_lb[j] = lb(j);
					_ub[j] = ub(j);
					_g[j] = g(j);
				}
			}
			_lbA[i] = lbA(i);
			_ubA[i] = ubA(i);
		}

		qpOASES::returnValue res = _solver.init(0, _g, _A, _lb, _ub, _lbA, _ubA, nWSR, 0);

		if (res == qpOASES::SUCCESSFUL_RETURN){
			qpOASES::real_t xOpt[nVariables];
			_solver.getPrimalSolution(xOpt);
			//printf("\nxOpt = [ %e, %e ];  objVal = %e\n\n", xOpt[0], xOpt[1], _solver.getObjVal());
			//std::cout << xOpt[0] << ", " << xOpt[1] << std::endl;
			for(int i=0; i<nVariables; i++){
				solution(i) = xOpt[i];
			}
			return true;
		}
		else{
			solution = VarVec::Zero();
			printf("[qpOASES] Failed to find solution");
			return false;
		}
	}

private:
	// determine objective of optimization
	qpOASES::QProblem _solver;
	bool _INITIAL_PROBLEM;
};
