////////////////////////////////////////////////////////////////////////////////
// C++ code for generation of cubic spline from a squence
// written in 2019-01-05 by D.Lee
////////////////////////////////////////////////////////////////////////////////
// Requirements (libraries)
// Eigen
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <Eigen/Eigen>
#include <vector>


template<int DIM>
class cSpline
{
private:
	typedef Eigen::Matrix<double, DIM, 1> vec;
	std::vector<vec> _Waypoint;
	int nPts;
	int nSplines;

	// parameter values at the connections of splines (s: 0~1)
	Eigen::VectorXd breaks;

	// coefficients: q(s) = a0*s^3 + a1*s^2 + a2*s + a3
	Eigen::MatrixXd a0;
	Eigen::MatrixXd a1;
	Eigen::MatrixXd a2;
	Eigen::MatrixXd a3;


private:
	void Interpolate() {
		breaks(0) = 0;
		Eigen::MatrixXd coeff_A;
		Eigen::MatrixXd coeff_b;
		Eigen::MatrixXd coeff;
		a0.resize(DIM, nSplines);
		a1.resize(DIM, nSplines);
		a2.resize(DIM, nSplines);
		a3.resize(DIM, nSplines);

		for (int j = 0; j < DIM; j++) {
			coeff_A = Eigen::MatrixXd::Zero(4 * nSplines, 4 * nSplines);
			coeff_b = Eigen::MatrixXd::Zero(4 * nSplines, 1);
			coeff = Eigen::MatrixXd::Zero(4 * nSplines, 1);
			for (int i = 0; i < nSplines; i++) {
				// N  = nSplines
				// N conditions: x = f_i(s_i)
				coeff_A(i, i * 4 + 0) = pow(i, 3);
				coeff_A(i, i * 4 + 1) = pow(i, 2);
				coeff_A(i, i * 4 + 2) = i;
				coeff_A(i, i * 4 + 3) = 1.0;
				coeff_b(i, 0) = _Waypoint[i][j];

				// N conditions: x = f_i(s_(i+1))
				coeff_A(nSplines + i, i * 4 + 0) = pow(i + 1, 3);
				coeff_A(nSplines + i, i * 4 + 1) = pow(i + 1, 2);
				coeff_A(nSplines + i, i * 4 + 2) = i + 1;
				coeff_A(nSplines + i, i * 4 + 3) = 1.0;
				coeff_b(nSplines + i, 0) = _Waypoint[i + 1][j];

				// 2*(N-1) conditions: f_i'(s_(i+1)) = f_(i+1)'(s_(i+1)), f_i''(s_(i+1)) = f_(i+1)''(s_(i+1))
				if (i < nSplines - 1) {
					coeff_A(2 * nSplines + i, i * 4 + 0) = 3.0*pow(i + 1, 2);
					coeff_A(2 * nSplines + i, i * 4 + 1) = 2.0*(i + 1);
					coeff_A(2 * nSplines + i, i * 4 + 2) = 1.0;
					coeff_A(2 * nSplines + i, (i + 1) * 4 + 0) = -3.0*pow(i + 1, 2);
					coeff_A(2 * nSplines + i, (i + 1) * 4 + 1) = -2.0*(i + 1);
					coeff_A(2 * nSplines + i, (i + 1) * 4 + 2) = -1.0;
					coeff_b(2 * nSplines + i, 0) = 0.0;

					coeff_A(3 * nSplines - 1 + i, i * 4 + 0) = 6.0*(i + 1);
					coeff_A(3 * nSplines - 1 + i, i * 4 + 1) = 2.0;
					coeff_A(3 * nSplines - 1 + i, (i + 1) * 4 + 0) = -6.0*(i + 1);
					coeff_A(3 * nSplines - 1 + i, (i + 1) * 4 + 1) = -2.0;
					coeff_b(3 * nSplines - 1 + i, 0) = 0.0;
				}

				// 2 conditions: f_0''(s_0) = 0, f_n''(s_(n+1)) = 0;
				coeff_A(4 * nSplines - 2, 0 * 4 + 0) = 6.0 * 0;
				coeff_A(4 * nSplines - 2, 0 * 4 + 1) = 2.0;
				coeff_b(4 * nSplines - 2, 0) = 0.0;

				coeff_A(4 * nSplines - 1, (nSplines - 1) * 4 + 0) = 6.0*nSplines;
				coeff_A(4 * nSplines - 1, (nSplines - 1) * 4 + 1) = 2.0;
				coeff_b(4 * nSplines - 1, 0) = 0.0;
			}
			
			// very slow!! need to find some other way!
			Eigen::HouseholderQR<Eigen::MatrixXd> QR_A(coeff_A);

			coeff = QR_A.solve(coeff_b);

			for (int i = 0; i < nSplines; i++) {
				a0(j, i) = coeff(4 * i);
				a1(j, i) = coeff(4 * i + 1);
				a2(j, i) = coeff(4 * i + 2);
				a3(j, i) = coeff(4 * i + 3);
			}
		}
	}

public:
	cSpline() {

	}

	cSpline(std::vector<vec> wp) : _Waypoint(wp), nPts(wp.size()), nSplines(wp.size() - 1)
	{
		a0.resize(DIM, nSplines);
		a1.resize(DIM, nSplines);
		a2.resize(DIM, nSplines);
		a3.resize(DIM, nSplines);
		breaks.resize(nPts);

		Interpolate();
	}

	vec eval(double s) {
		assert(("parameter should be within range", s <= nSplines));
		int idx;
		if (s < nSplines) {
			idx = (int)floor(s);
		}
		else {
			idx = nSplines - 1;
		}

		vec res(DIM);
		for (int i = 0; i < DIM; i++) {
			res(i) = a0(i, idx)*pow(s, 3) + a1(i, idx)*pow(s, 2) + a2(i, idx)*s + a3(i, idx);
		}
		return res;
	}

	vec eval_d(double s) {
		assert(("parameter should be within range", s <= nSplines));
		int idx;
		if (s < nSplines) {
			idx = (int)floor(s);
		}
		else {
			idx = nSplines - 1;
		}

		vec res(DIM);
		for (int i = 0; i < DIM; i++) {
			res(i) = 3.0*a0(i, idx)*pow(s, 2) + 2.0*a1(i, idx)*s + a2(i, idx);
		}
		return res;
	}

	vec eval_dd(double s) {
		assert(("parameter should be within range", s <= nSplines));
		int idx;
		if (s < nSplines) {
			idx = (int)floor(s);
		}
		else {
			idx = nSplines - 1;
		}

		vec res(DIM);
		for (int i = 0; i < DIM; i++) {
			res(i) = 6.0*a0(i, idx)*s + 2.0*a1(i, idx);
		}
		return res;
	}

	int max_param() {
		return nSplines;
	}
};
