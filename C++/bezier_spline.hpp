////////////////////////////////////////////////////////////////////////////////
// C++ code for generation of bezier spline from a squence
// written in 2019-01-06 by D.Lee
// Reference: https://denisrizov.com/2016/06/02/bezier-curves-unity-package-included/
////////////////////////////////////////////////////////////////////////////////
// Requirements (libraries)
// Eigen
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <Eigen/Eigen>
#include <vector>

// // modified from reference code
template<typename T>
T Lerp(T a, T b, T t) {
	return (1.0 - t) * a + t * b;
}

// modified from reference code
template<typename T>
T GetPointOnBezierCurve(T p0, T p1, T p2, T p3, double t) {
	T result = p0 * pow(1.0 - t, 3) + p1 * 3.0*pow(1.0 - t, 2)*t + p2 * 3.0*pow(t, 2)*(1.0 - t) + p3 * pow(t, 3);
	return result;
}

template<typename T>
T GetDerivativeOnBezierCurve(T p0, T p1, T p2, T p3, double t) {
	T result = p0 * -3.0*pow(1.0 - t, 2) + p1 * 3.0*(1.0 - 4.0*t + 3.0*pow(t, 2)) + p2 * 3.0*(-3.0*pow(t, 2) + 2.0*t) + p3 * 3.0*pow(t, 2);
	return result;
}


template<typename T>
T GetDDerivativeOnBezierCurve(T p0, T p1, T p2, T p3, double t) {
	T result = p0 * 6.0*(1.0 - t) + p1 * 6.0*(3.0*t - 2.0) + p2 * 6.0*(-3.0*t + 1.0) + p3 * 6.0*t;
	return result;
}

template<int DIM>
class bSpline {
private:
	typedef Eigen::Matrix<double, DIM, 1> vec;

	enum{
		MAX_WAYPOINT_NUM = 1000,
	};
	int _nCtrlPts;
	int _nSplines;
	//std::vector<vec> _waypoints;
	vec _waypoints[MAX_WAYPOINT_NUM];
	int _waypoint_num;
	//std::vector<vec> _ctrl_pts;
	vec _ctrl_pts[3*MAX_WAYPOINT_NUM+1];

	void set_ctrl_points(bool CURVE_CLOSED) {
		if (CURVE_CLOSED) {
			vec ctrl_pt0, ctrl_pt1, ctrl_pt2;
			//int nPts = _waypoints.size();
			int nPts = _waypoint_num;
			ctrl_pt0 = _waypoints[nPts - 1] * 1.0 / 3.0 + _waypoints[0] * 2.0 / 3.0;
			ctrl_pt2 = _waypoints[0] * 2.0 / 3.0 + _waypoints[1] * 1.0 / 3.0;
			ctrl_pt1 = (ctrl_pt0 + ctrl_pt2) / 2.0;
			_ctrl_pts[0] = ctrl_pt1;
			_ctrl_pts[1] = ctrl_pt2;
			for (int i = 0; i < nPts - 2; i++) {
				vec ctrl_pt3, ctrl_pt4, ctrl_pt5;
				ctrl_pt3 = _waypoints[i] * 1.0 / 3.0 + _waypoints[i + 1] * 2.0 / 3.0;
				ctrl_pt5 = _waypoints[i + 1] * 2.0 / 3.0 + _waypoints[i + 2] * 1.0 / 3.0;
				ctrl_pt4 = (ctrl_pt3 + ctrl_pt5) / 2.0;
				_ctrl_pts[2+3*i] = ctrl_pt3;
				_ctrl_pts[3+3*i] = ctrl_pt4;
				_ctrl_pts[4+3*i] = ctrl_pt5;
			}
			vec ctrl_pt6, ctrl_pt7, ctrl_pt8;
			ctrl_pt6 = _waypoints[nPts - 2] * 1.0 / 3.0 + _waypoints[nPts - 1] * 2.0 / 3.0;
			ctrl_pt8 = _waypoints[nPts - 1] * 2.0 / 3.0 + _waypoints[0] * 1.0 / 3.0;
			ctrl_pt7 = (ctrl_pt6 + ctrl_pt8) / 2.0;
			_ctrl_pts[3*nPts-4] = ctrl_pt6;
			_ctrl_pts[3*nPts-3] = ctrl_pt7;
			_ctrl_pts[3*nPts-2] = ctrl_pt8;
			_ctrl_pts[3*nPts-1] = ctrl_pt0;
			_ctrl_pts[3*nPts] = ctrl_pt1;

			_nCtrlPts = 3 * nPts + 1;
			_nSplines = nPts;
		}
		else {
    		//int nPts = _waypoints.size();
			int nPts = _waypoint_num;

			_ctrl_pts[0] = _waypoints[0];
			_ctrl_pts[1] = _waypoints[0] * 2.0 / 3.0 + _waypoints[1] * 1.0 / 3.0;

			for (int i = 0; i < nPts - 2; i++) {
				vec ctrl_pt1, ctrl_pt2, ctrl_pt3;
				ctrl_pt1 = _waypoints[i] * 1.0 / 3.0 + _waypoints[i + 1] * 2.0 / 3.0;
				ctrl_pt3 = _waypoints[i + 1] * 2.0 / 3.0 + _waypoints[i + 2] * 1.0 / 3.0;
				ctrl_pt2 = (ctrl_pt1 + ctrl_pt3) / 2.0;
				_ctrl_pts[2+3*i] = ctrl_pt1;
				_ctrl_pts[3+3*i] = ctrl_pt2;
				_ctrl_pts[4+3*i] = ctrl_pt3;
			}

			_ctrl_pts[3*nPts-4] = _waypoints[nPts - 2] * 1.0 / 3.0 + _waypoints[nPts - 1] * 2.0 / 3.0;
			_ctrl_pts[3*nPts-3] = _waypoints[nPts - 1];

			_nCtrlPts = 3 * nPts - 2;
			_nSplines = nPts - 1;
		}

		//for (int i =0;i<_nCtrlPts;i++){
		//	std::cout << i << ": " << _ctrl_pts[i] << std::endl;
		//}
	}

public:
	bSpline() {
	}

	/*
	bSpline(std::vector<vec> wps, bool closed_curve = false):
		_waypoints(wps)
	{
		printf("check1 \r\n");
		set_ctrl_points(closed_curve);
	}
	*/

	bSpline(vec* wps, int nPts, bool closed_curve = false)
	{
		if(nPts>MAX_WAYPOINT_NUM){
			nPts = MAX_WAYPOINT_NUM;
			printf("[WARNING] The number of waypoints exceeds maximum number \r\n");
		}

		_waypoints[0] = wps[0];
		// enlist waypoints except repeating points
		int idx = 1;
		for(int i=1; i<nPts;i++){
			if(wps[i-1]==wps[i]){
				continue;
			}
			_waypoints[idx] = wps[i];
			idx++;
		}
		_waypoint_num = idx;
		printf("waypoints: %d \r\n", idx);
		set_ctrl_points(closed_curve);
	}

	vec eval(double s) {
		assert(("parameter should be within range", s <= _nSplines && s >= 0));
		int idx;
		if (s < _nSplines) {
			idx = (int)floor(s);
		}
		else {
			idx = _nSplines - 1;
		}

		vec res, ctrl_pt0, ctrl_pt1, ctrl_pt2, ctrl_pt3;
		ctrl_pt0 = _ctrl_pts[idx * 3];
		ctrl_pt1 = _ctrl_pts[idx * 3 + 1];
		ctrl_pt2 = _ctrl_pts[idx * 3 + 2];
		ctrl_pt3 = _ctrl_pts[idx * 3 + 3];

		res = GetPointOnBezierCurve(ctrl_pt0, ctrl_pt1, ctrl_pt2, ctrl_pt3, s - idx);
		return res;
	}

	vec eval_d(double s) {
		assert(("parameter should be within range", s <= _nSplines && s >= 0));
		int idx;
		if (s < _nSplines) {
			idx = (int)floor(s);
		}
		else {
			idx = _nSplines - 1;
		}

		vec res, ctrl_pt0, ctrl_pt1, ctrl_pt2, ctrl_pt3;
		ctrl_pt0 = _ctrl_pts[idx * 3];
		ctrl_pt1 = _ctrl_pts[idx * 3 + 1];
		ctrl_pt2 = _ctrl_pts[idx * 3 + 2];
		ctrl_pt3 = _ctrl_pts[idx * 3 + 3];

		res = GetDerivativeOnBezierCurve(ctrl_pt0, ctrl_pt1, ctrl_pt2, ctrl_pt3, s - idx);
		return res;
	}

	vec eval_dd(double s) {
		assert(("parameter should be within range", s <= _nSplines && s >= 0));
		int idx;
		if (s < _nSplines) {
			idx = (int)floor(s);
		}
		else {
			idx = _nSplines - 1;
		}

		vec res, ctrl_pt0, ctrl_pt1, ctrl_pt2, ctrl_pt3;
		ctrl_pt0 = _ctrl_pts[idx * 3];
		ctrl_pt1 = _ctrl_pts[idx * 3 + 1];
		ctrl_pt2 = _ctrl_pts[idx * 3 + 2];
		ctrl_pt3 = _ctrl_pts[idx * 3 + 3];

		res = GetDDerivativeOnBezierCurve(ctrl_pt0, ctrl_pt1, ctrl_pt2, ctrl_pt3, s - idx);
		return res;
	}

	void check(){
		for (int i =0;i<_nCtrlPts;i++){
			std::cout << i << std::endl << _ctrl_pts[i] << std::endl;
		}
	}

	int maxParam() {
		return _nSplines;
	}
};